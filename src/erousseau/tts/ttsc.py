import argparse
import logging
from typing import List
from erousseau.tts.codec.writters import BinaryWriter
from erousseau.tts.codec.codepageexport import export_code_page

from erousseau.tts.codec.codec import Codec, CoderBuilder

from erousseau.tts.codec.codepage import CodePage, TextData, guess_codepage

logger = logging.getLogger(__name__)


def _load_text_input(filepath: str) -> List[str]:
    with open(filepath, "rt") as f:
        all_lines: List[str] = f.readlines()

    # removes empty lines
    cleaned_lines: List[str] = []
    for l in all_lines:
        l = l.strip()
        if l == "":
            continue

        cleaned_lines.append(l)

    return cleaned_lines


def compress(in_filepath: str, out_filepath: str, verbose: bool = False):
    # load file text lines
    txt_lines: List[str] = _load_text_input(in_filepath)
    # figure out the codepage to use (LATIN only supported atm)
    codepage: CodePage = guess_codepage(txt_lines)

    # lets build a dataset for the text
    textdata: TextData = codepage.new_text_data()
    textdata.append_text(txt_lines)

    # creates a coder using the dataset
    coder_builder: CoderBuilder = CoderBuilder(textdata, verbose)
    coder: Codec = coder_builder.build()

    writer: BinaryWriter = BinaryWriter(out_filepath, coder.get_dict_data())

    for s in textdata._tokenized_lines:
        writer.add_sentence(coder.encode(s))

    writer.write()
    export_code_page(".", codepage)


def main() -> None:
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        description="""
    This tool reads a text file and output a compressed file
    encoded in ctxt.\n
    """
    )

    parser.add_argument("-o", "--output", dest="output", type=str,
                        help="output", default="./out.ctxt")

    parser.add_argument("-v", "--verbose", dest="verbose",
                        help="verbosity", action="store_true")

    parser.add_argument(
        "input",
        type=str,
        help="file to compress",
    )
    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # compress(args.input, args.output)
    compress(args.input, args.output, args.verbose)


if __name__ == "__main__":
    main()
