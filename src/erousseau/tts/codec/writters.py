from typing import List
from erousseau.tts.codec.codec import CodecDictionnaryData

BIN_FILE_ID = b"XTTC"


def int_to_lit32(v: int) -> bytearray:
    arr = bytearray(4)
    m = memoryview(arr)
    m[3] = (v >> 24) & 0xFF
    m[2] = (v >> 16) & 0xFF
    m[1] = (v >> 8) & 0xFF
    m[0] = v & 0xFF

    return arr


def int_to_lit16(v: int) -> bytearray:
    arr = bytearray(2)
    m = memoryview(arr)
    m[1] = (v >> 8) & 0xFF
    m[0] = v & 0xFF

    return arr


def int_to_big32(v: int) -> bytearray:
    arr = bytearray(4)
    m = memoryview(arr)
    m[0] = (v >> 24) & 0xFF
    m[1] = (v >> 16) & 0xFF
    m[2] = (v >> 8) & 0xFF
    m[3] = v & 0xFF

    return arr


def int_to_big16(v: int) -> bytearray:
    arr = bytearray(2)
    m = memoryview(arr)
    m[0] = (v >> 8) & 0xFF
    m[1] = v & 0xFF

    return arr


class BinaryWriter:
    def __init__(self, filepath: str, codecdata: CodecDictionnaryData) -> None:
        self._filepath = filepath
        self._codec_data: CodecDictionnaryData = codecdata
        self._sentences: List[List[int]] = []

        self._version = b"\x01"

    def add_sentence(self, sentence: List[int]) -> None:
        self._sentences.append(sentence)

    def write(self) -> None:
        with open(self._filepath, "wb") as f:
            f.write(BIN_FILE_ID)
            f.write(self._version)

            f.write(int_to_big16(len(self._codec_data.ngram_offsets)))
            f.write(int_to_big16(len(self._codec_data.word_offsets)))
            f.write(int_to_big32(len(self._sentences)))

            address_addr: int = f.tell()

            f.write(int_to_big32(0))
            f.write(int_to_big32(0))
            f.write(int_to_big32(0))
            f.write(int_to_big32(0))
            f.write(int_to_big32(0))
            f.write(int_to_big32(0))

            ngram_offset_addr: int = f.tell()
            f.write(bytearray(self._codec_data.ngram_offsets))

            word_offsets_addr: int = f.tell()
            f.write(bytearray(self._codec_data.word_offsets))

            ngram_dict_addr: int = f.tell()
            f.write(bytearray(self._codec_data.ngram_dict))

            word_dict_addr: int = f.tell()
            f.write(bytearray(self._codec_data.word_dict))

            sentence_page_addr: List[int] = [f.tell()]
            sentence_count_per_page: List[int] = [0]

            sentence_index: List[int] = []
            for s in self._sentences:
                sentence_bytes = bytearray(s)
                f.write(sentence_bytes)

                sentence_count_per_page[-1] += 1

                bytelen: int = f.tell() - sentence_page_addr[-1]
                sentence_index.append((bytelen >> 8) & 0xFF)
                sentence_index.append(bytelen & 0xFF)

                if f.tell() - sentence_page_addr[-1] >= 0xFFFF:
                    sentence_page_addr.append(f.tell())
                    sentence_count_per_page.append(0)

            # write the end address of all pages (or last page)
            sentence_page_addr.append(f.tell())
            sentence_count_per_page.append(0)

            sentence_offset_addr: int = f.tell()
            f.write(bytearray(sentence_index))

            sentence_index_addr: int = f.tell()

            f.write(int_to_big16(len(sentence_count_per_page)))
            for addr, count in zip(sentence_page_addr, sentence_count_per_page):
                f.write(int_to_big32(addr))
                f.write(int_to_big16(count))

            f.seek(address_addr)

            f.write(int_to_big32(ngram_offset_addr))
            f.write(int_to_big32(word_offsets_addr))
            f.write(int_to_big32(ngram_dict_addr))
            f.write(int_to_big32(word_dict_addr))
            f.write(int_to_big32(sentence_index_addr))
            f.write(int_to_big32(sentence_offset_addr))
