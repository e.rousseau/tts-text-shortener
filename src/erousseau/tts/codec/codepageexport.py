import os
from erousseau.tts.c.c_module import CLangModule
from erousseau.tts.c.c_header import CLangHeader
from csnake import Variable
import logging
from typing import List
logger = logging.getLogger(__name__)


def export_code_page(dirpath, cp) -> None:
    codepage_name = f"codepage_{cp.name.lower()}"

    c_header_name = f"c_{codepage_name}.h"
    c_module_name = f"c_{codepage_name}.c"

    c_header_filepath: str = os.path.join(dirpath, "inc", c_header_name)
    c_module_filepath: str = os.path.join(dirpath, "src", c_module_name)

    logger.info(f"writing {c_header_filepath}")
    logger.info(f"writing {c_module_filepath}")

    c_header: CLangHeader = CLangHeader(str(c_header_filepath))
    c_module: CLangModule = CLangModule(str(c_module_filepath))

    # indexing the charset
    charset: str = cp.charset
    indexed_charset = {}

    for i, char in enumerate(charset):
        indexed_charset[char] = i + 32

    # compute offsets for compressed charset (127 based codepage)
    output_charset = cp.output_charset
    output_offsets = []

    for i, char in enumerate(output_charset):
        if char not in indexed_charset:
            mapped_index = 0
        else:
            mapped_index = indexed_charset[char]

        output_offsets.append(mapped_index)

    # compute the uppercase mapping
    output_uppercase_map = []
    for c in charset:
        cupper = c.upper()
        if c == cupper or cupper not in indexed_charset:
            output_uppercase_map.append(indexed_charset[c])
            continue
        output_uppercase_map.append(indexed_charset[cupper])

    charset_var_name = f"{codepage_name}_charset"
    charset_vram_name = f"{codepage_name}_charset_vram"
    charset_uppercase_map = f"{codepage_name}_uppercase_map"

    charset_var: Variable = Variable(
        charset_var_name,
        primitive="u8",
        qualifiers="const",
        value=output_offsets,
    )

    charset_vram_offsets: List[int] = []
    for i, _ in enumerate(charset):
        charset_vram_offsets.append(i + 32)

    charset_vram_var: Variable = Variable(
        charset_vram_name,
        primitive="u8",
        qualifiers="const",
        value=charset_vram_offsets,
    )

    charset_uppercase_map_var: Variable = Variable(
        charset_uppercase_map,
        primitive="u8",
        qualifiers="const",
        value=output_uppercase_map,
    )

    c_module.include("i_base.h")
    c_module.include(c_header.get_include_filename())

    c_module.add_variable_initialization(charset_var)
    c_module.add_variable_initialization(charset_vram_var)
    c_module.add_variable_initialization(charset_uppercase_map_var)

    c_header.declare(charset_var)
    c_header.declare(charset_vram_var)
    c_header.declare(charset_uppercase_map_var)

    c_module.write()
    c_header.write()
