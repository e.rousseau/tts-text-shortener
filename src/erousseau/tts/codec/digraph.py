from __future__ import annotations
from typing import Any, Generator, List, Mapping, Tuple


def seq() -> int:
    i = 0

    while True:
        yield i
        i += 1


s = seq()


class Node:
    def __init__(self, value: Any, parent: Node) -> None:
        self.value: Any = value
        self.id = next(s)
        self.visited = 0
        self.children: Mapping[Any, Node] = {}
        self.parent: Node = parent

    def merge(self, s):
        if s[0] != self.value:
            raise RuntimeError("can't be merged")

        s = s[1:]

        if not s:
            return

        if s[0] not in self.children:
            self.children[s[0]] = Node(s[0], self)

        self.visited += 1
        self.children[s[0]].merge(s)

    def all_parents(self) -> Generator[Node, None, None]:
        """
        will return every node up to root the None
        """
        p: Node = self.parent

        while p is not None:
            yield p
            p = p.parent

    def __eq__(self, v) -> bool:
        if v == None:
            return False
        if isinstance(v, Node):
            return v.value == self.value
        else:
            raise RuntimeError(f"not a node, {type(v)}")

    def __hash__(self) -> int:
        return hash(f"{self.value}_{self.id}")


class DiGraph:
    def __init__(self) -> None:
        self.first: Node = Node("Root", None)

    def merge(self, s):
        if not s:
            return

        v0 = s[0]
        if v0 not in self.first.children:
            self.first.children[v0] = Node(v0, self.first)

        self.first.children[v0].merge(s)
        self.merge(s[1:])

    def as_bigrams(self) -> List[Tuple[Node, Node]]:
        """
        bigrams are sequences of two characters Nodes in the tree
        """
        bigrams: List[Tuple[Node, Node]] = []

        # breath first traversal
        stack: List[Node] = [self.first]
        while len(stack):
            n: Node = stack.pop()
            for c in n.children.values():
                bigram: Tuple[Node, Node] = (n, c)
                bigrams.append(bigram)
                stack.append(c)

        return bigrams

    def __str__(self) -> str:
        # parsable by graphviz, dot notation
        s = "digraph A {\n"

        grams2: List[Tuple[Node, Node]] = self.as_2grams()
        for gram2 in grams2:
            n1, n2 = gram2
            s += f'"{n1.value}_{n1.id}_{n1.visited}" -> "{n2.value}_{n2.id}_{n2.visited}"\n'

        s += "}\n"
        return s
