from __future__ import annotations
from typing import List, Mapping, Set, Tuple, Generator
from erousseau.tts.codec.digraph import DiGraph, Node
from erousseau.tts.codec.codepage import CodePage, TextData
import erousseau.tts.codec.lists as lists


class CodecError(RuntimeError):
    pass


class CoderBuilder:
    def __init__(self, textdata, verbose: bool = False):
        self._codepage: CodePage = textdata.codepage()
        self._textdata: TextData = textdata

        # token characterization fields
        self._compactable_tokens: Mapping[str, int] = {}
        self._litteral_tokens: Mapping[str, int] = {}
        self._verbose: bool = verbose

    def build(self) -> Codec:
        self._characterize_tokens()
        ngramdict: List[Nibble] = self._new_ngramdict()
        worddict: List[Nibble] = self._new_worddict(ngramdict)

        return Codec(self._codepage, ngramdict, worddict, self._verbose)

    def _characterize_tokens(self):
        """
        split tokens into two family, tokens that cannot be reduced and
        must be kept as litteral data. No way to handle them to save space
        but, lets hope, they are scarse. Other tokens are compressible. See
        the README.md for more details.
        """
        for token in self._textdata.tokens():
            compactable: bool = len(token) == 1 or self._codepage.is_compactable(
                token[1:]
            )
            if not compactable and not self._codepage.is_nonword(token):
                if token in self._litteral_tokens:
                    self._litteral_tokens[token] += 1
                else:
                    self._litteral_tokens[token] = 1
                continue

            if compactable and self._codepage.is_nonword(token):
                # print(token, compactable, self._codepage.is_nonword(token))
                if token in self._litteral_tokens:
                    self._litteral_tokens[token] += 1
                else:
                    self._litteral_tokens[token] = 1
                continue

            token = token.casefold()
            if token in self._compactable_tokens:
                self._compactable_tokens[token] += 1
            else:
                self._compactable_tokens[token] = 1

    def _new_worddict(self, ngramdict: List[Nibble]) -> List[Nibble]:
        """
        _new_worddict generates a L2 compression dictionnary. This dictionnary
        indexes all the tokens found during the characterization phase. It builds
        up a list of these tokens in descending order of frequency they appears
        in the text. The ordering is quite important, because the first 127
        entries have a special spacial property, the reference consumes a single
        bytes instead of two bytes. All compactable tokens are folded using the
        ngramdict.

        This dictionnary has the higest compression gain on a text input as
        a text tends to have few words (typicaly less than 3000) but large
        arrangement of them. By replacing these words by offsets to
        dictionnary entries, we get a pretty high compression effect.
        """
        nibbles: List[Nibble] = []

        # convert the tokens into dictionnary Nibbles
        # tokens having a frequency of 1 are excluded to unclutter
        # the dictionnary and they gonna be encoded as litteral data
        # when sentences are going to be compressed/encoded.
        for token, frequency in self._compactable_tokens.items():
            if frequency <= 1:
                continue
            nibble: Nibble = Nibble(self._codepage, token, frequency)
            nibbles.append(nibble)

        # convert non compactable tokens into litteral nibbles. Same here
        # for the frequency stuff.
        for token, frequency in self._litteral_tokens.items():
            if frequency <= 1:
                continue
            nibble: Nibble = Nibble(
                self._codepage, token, frequency, litteral=True)
            nibbles.append(nibble)

        nibbles = sorted(nibbles, key=lambda n: n.frequency, reverse=True)

        # fold the worddict with the ngramdict
        for nibble in nibbles:
            nibble.fold(ngramdict)

        return nibbles

    def _new_ngramdict(self) -> List[Nibble]:
        """
        _new_ngramdict builds a L1 compression dictionnary. This dictionnary has
        only 256 bytes and contains very small groups of letters called here
        {2-5}grams, n-grams. (https://en.wikipedia.org/wiki/N-gram)

        This dictionnary is folded on itself, meaning, this ngramdict removes
        redundancy within itself progressively. The top most present n-grams
        are kept.

        It's then used to reduce the size of the data in the word dictionnary.
        The byte encoding is the following:

        0 : reserved
        1-127 : typicaly a lower case letter (depends on the CodePage)
        128-255 : refers to a n-gram encoded in this dictionnary.

        Compressing ngramdict seems devoid of interrest given its small size,
        but it enables to pack even more n-grams, thus, it has a big impact on
        the word dictionnary size later.

        This data structure, given best case situation may chew up 7% of total
        size economy if conditions are aligned.

        NOTE : This algorithm could probably be written as dynamic programming
        if someone find a way to decompose the problem into simple sub problems.
        For the moment, the digraph approach works but isn't very memory
        efficient. (who cares maybe?)
        """

        # lets build the digraph with all the distinct words.
        # TODO : add a word count notions to increment the visit relative to
        # the number of time a string sequence has been met. s.merge(w, count)
        d = DiGraph()
        for w in self._compactable_tokens.keys():
            d.merge(w)

        bigrams: List[Tuple[Node, Node]] = d.as_bigrams()

        if not bigrams:
            # no compactable token, weird... but lets exit this.
            return []

        # lets build a descending list of all the frequencies
        all_visited: List[int] = [n2.visited for _, n2 in bigrams]
        all_visited.sort(reverse=True)

        # pick a visited value that has statistical significance, the
        # compression dictionnary cannot have all the character sequences
        # TODO: statistical analysis here could be helpful
        index: int = int(0.1 * len(all_visited))
        index = max(len(all_visited) - 1, index)
        threshold: int = all_visited[index]

        # filters out all the bigrams that are under the threshold for visits
        nodes: Generator[Node, None, None] = (
            n2 for _, n2 in bigrams if n2.visited > threshold
        )
        # remove duplicates
        common_nodes: Set[Node] = set(nodes)

        # in the digraph, the visited value decrease with the distance from the
        # root. Meaning, most leaves went removed. This property can be used
        # to easily find the new leaves in this reduces set. If the children
        # of a given node has no members in common_nodes, it's a leaf.
        leaves: List[Node] = [
            n for n in common_nodes if not common_nodes.isdisjoint(n.children.values())
        ]

        nibbles: List[Nibble] = []

        # using every leaf, lets reconstruct the character sequences
        for leaf in leaves:
            charseq: str = leaf.value

            for l in leaf.all_parents():
                if l.value == "Root":
                    break
                charseq = l.value + charseq

            nibble: Nibble = Nibble(self._codepage, charseq, leaf.visited)
            nibbles.append(nibble)

        # lets remove all the single char nibbles, they occupy space
        # but doesn't offer any gain compared to a litteral character
        a = filter(
            lambda n: n.length() >= L1_DICT_MIN_NIBBLE_LEN
            and n.length() <= L1_DICT_MAX_NIBBLE_LEN,
            nibbles,
        )
        b = sorted(a, key=lambda n: n.frequency, reverse=True)

        # using here FAST_DICT_MAX_SIZE/2 should cut enough data to speed up
        # self folding
        nibbles = list(b)[: int(L1_DICT_MAX_SIZE / 2)]
        # lets sort by length
        nibbles = list(sorted(nibbles, key=lambda n: n.length()))

        # lets reduce entropy of the fast dict
        for nibble in nibbles:
            nibble.fold(nibbles)

        # keep at most 256 bytes of this
        result: List[Nibble] = []
        acc_size: int = 0

        for n in nibbles:
            acc_size += n.length()
            if acc_size >= L1_DICT_MAX_SIZE:
                break
            result.append(n)

        return result


L1_DICT_MAX_SIZE: int = 256
L1_DICT_MIN_NIBBLE_LEN: int = 2
L1_DICT_MAX_NIBBLE_LEN: int = 5

# special reserved code used to indicate litteral content in the stream
LITTERAL_CODE = 0

# when on, means first letter of the word must be capitalized by
# the decoder
CAPITALIZED_FLAG = 0x10
# when bit 6-7 = 01, means, decoder must add a comma before addind a space
PERIOD_FLAG = 0x20
# when bit 6-7 = 10, means, decoded must add a period before addind a space
COMMA_FLAG = 0x40
# when bit 6-7 = 11, undefined for the moment. Usable for extensions to
# compression scheme
UNUSED_FLAG = 0x60

# when bit 8 is on, it means the offset of a word is over 12bit. This set
# the limit of total addressable words to 4095 + 127.
#
# when bit 8 is off, word offset is encoded on 7 bits thus, 127 most
# common words of the dictionnary encode their offset on only one byte.
# Lets remember though that 0 is reserved for LITTERAL_CODE, thus 127 words
# and not 128.
WORD_OFFSET_FLAG = 0x80


class Nibble:
    """
    a nibble is the working unit of the Codec. It links the String data to
    compressed raw data.
    """

    def __init__(
        self, codepage: CodePage, txt: str, frequency: int, litteral: bool = False
    ):
        # actual data
        self._data: List[int] = []
        # text representation of the data. Beware, it might not match 1on1 when
        # folding occured
        self._txt: str = txt
        # weight of this nibble. Represents some form of redundancy value
        self.frequency: int = frequency
        # when true, the Nibble is litteral. Litteral Nibble cannot be fold
        # and aren't encoded using the output_charset, but with the complete
        # charset of the codepage.
        self.litteral: bool = litteral

        if litteral:
            self._data.append(LITTERAL_CODE)
            l = codepage.to_litteral_ints(txt)
            self._data.extend(l)
        else:
            self._data.extend(codepage.to_ints(txt))

    def txt_value(self) -> str:
        """
        returns the text representation of this nibble.
        """
        return self._txt

    def encoded_value(self) -> List[int]:
        """
        return the Nibble actual encoded data.
        """
        return self._data

    def length(self):
        """
        returns the length of the nibble in byte.
        """
        return len(self._data)

    def fold(self, nibbles: List[Nibble]):
        """
        try to remove repetitions in the compacted_data and replace the sequence
        with the nibble id. Folding is done with the ngramdict.
        """
        if self.litteral:
            return

        for nib_index, nibble in enumerate(nibbles):
            if nibble is self:
                # doesn't make sense to fold a nibble with itself
                break

            if nibble.litteral:
                continue

            ref: List[int] = (
                i for i in lists.search_sub_array(self._data, nibble._data)
            )
            new_compacted = []

            skip: int = 0
            try:
                ref_index: int = next(ref)
            except StopIteration:
                continue

            for i, c in enumerate(self._data):
                if skip:
                    skip -= 1
                    continue

                if i == ref_index:
                    # replace sequence of chars with a reference to
                    # nibble at a position
                    new_compacted.append(0x80 | nib_index)
                    # len(nibble.txt_value()) - 1
                    skip += len(nibble._data) - 1
                else:
                    new_compacted.append(c)

            self._data = new_compacted

    def __repr__(self) -> str:

        return f"Nibble({self._txt}, {self.frequency}, {'litteral' if self.litteral else 'compacted'}, {self._data})"


class Codec:
    def __init__(
        self,
        codepage: CodePage,
        ngramdict: List[Nibble],
        worddict: List[Nibble],
        verbose: bool = False,
    ):
        self._codepage: CodePage = codepage
        self._ngramdict: List[Nibble] = ngramdict
        self._worddict: Mapping[str, Nibble] = {
            w.txt_value(): w for w in worddict}
        self._wordpos: Mapping[Nibble, int] = {
            n: i for i, n in enumerate(worddict)}

        self._verbose = verbose

    def encode(self, sentence: List[str]) -> List[int]:
        out: List[int] = []
        # if last token is a period, leave it out. Every sentence
        # ends by a period. XXX: This behavior should be configurable
        # or detected.
        if sentence[-1] == ".":
            sentence = sentence[: len(sentence) - 1]

        flags: int = 0
        for wordpos, token in enumerate(sentence):
            if flags & (COMMA_FLAG | PERIOD_FLAG):
                # we encoded a comma or a period in the word, lets skip it.
                flags = 0
                continue
            else:
                flags = 0

            nibble: Nibble = None
            # search for a Nibble in the word dictionnary
            if token in self._worddict:
                nibble = self._worddict[token]
            else:
                t: str = token.casefold()
                if t in self._worddict:
                    nibble = self._worddict[t]

                    if (
                        not self._codepage.is_first_letter_compactable(token)
                        and wordpos > 0
                    ):
                        flags |= CAPITALIZED_FLAG

            # if the word is not the last word of the sentence (punctuation excluded)
            if wordpos < len(sentence) - 2:
                next_token: str = sentence[wordpos + 1]
                # XXX: it could be usefull to guess the punctuation instead
                # of hardcoding that. Needs more tests to ensure its the
                # way to go. FF6 script uses a lot of "!", maybe encoding "!" is
                # better than "."
                if next_token == ",":
                    flags |= COMMA_FLAG
                elif next_token == ".":
                    flags |= PERIOD_FLAG

            if nibble is None:
                # not found token, lets encode litterally in Sentence
                out.append(LITTERAL_CODE)
                # write the len of the string
                if len(token) >= 32:
                    # FIXME: we should push word with length greater than 32 in
                    # the word dictionnary
                    raise CodecError(
                        "can't encode 32 bytes or longer litterals")
                out.append(len(token) | (flags << 1))
                # write the bytes
                out.extend(self._codepage.to_litteral_ints(token))
            else:
                wordindex: int = self._wordpos[nibble]

                if not flags and wordindex < 0x7F:
                    out.append(wordindex + 1)
                else:
                    wordindex += 1
                    flags |= WORD_OFFSET_FLAG
                    u: int = ((wordindex >> 8) & 0xF) | flags
                    l: int = wordindex & 0xFF
                    out.append(u)
                    out.append(l)

        return out

    def get_dict_data(self) -> CodecDictionnaryData:
        """
        when called, the datablocks for the dictionnaries are generated and
        pushed to the datawritter.
        """

        ngram_offsets: List[int] = []
        ngrams: List[int] = []

        i: int = 0
        for n in self._ngramdict:
            if self._verbose:
                print(n)

            i += n.length()

            if i > 0xFF:
                total_required_space: int = sum(
                    (m.length() for m in self._ngramdict))

                raise CodecError(
                    "ngrams dictionnary space is too small, "
                    f"{total_required_space} bytes are required but max is 256. "
                    "Obviously this is a bug."
                )

            ngram_offsets.append(i)
            ngrams.extend(n.encoded_value())

        words: List[int] = []
        word_offsets: List[int] = []

        if self._verbose:
            print("================== WORDS")

        i = 0
        for n in self._worddict.values():
            if self._verbose:
                print(n)

            i += n.length()

            if i > 0xFFFF:
                total_required_space: int = sum(
                    (m.length() for m in self._worddict.keys())
                )

                raise CodecError(
                    "dictionnary space for words is too small, "
                    f"{total_required_space} bytes are required but max is 65536. "
                    "Input text vocabulary is probably either too much varied "
                    "or contains a lot of very long words."
                )

            word_offsets.append((i >> 8) & 0xFF)
            word_offsets.append(i & 0xFF)

            words.extend(n.encoded_value())

        return CodecDictionnaryData(ngrams, ngram_offsets, words, word_offsets)


class CodecDictionnaryData:
    """
    transfert object containing dictionnary data in a byte form. This object
    is for output purpose only.
    """

    def __init__(
        self,
        ngrams_dict: List[int],
        ngram_offsets: List[int],
        word_dict: List[int],
        word_offsets: List[int],
    ):
        self.ngram_dict: List[int] = ngrams_dict
        self.ngram_offsets: List[int] = ngram_offsets
        self.word_dict: List[int] = word_dict
        self.word_offsets: List[int] = word_offsets
