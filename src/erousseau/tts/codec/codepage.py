from __future__ import annotations
import logging
import re
from typing import Iterable, List, Mapping, Set, Generator

logger = logging.getLogger(__name__)


class CodePageError(RuntimeError):
    pass


class CodePage:
    def __init__(
        self, name: str, charset: str, output_charset: str, word_chars: str
    ) -> None:
        """
        instanciate CodePage with a name. The charset is a string containing
        all the characters accepted by this codepage. The position of the
        characters in the charset define the final encoding in byte.

        word_chars defines the symbols that are part of a word token. We usualy
        want pronounciable symbols and exclude punctuation etc...
        """
        self.name = name
        self.charset: str = charset
        self.word_chars: str = word_chars
        self._wordtoken_pattern = re.compile(f"([{word_chars}]+)")
        self._nonwordtoken_pattern = re.compile(f"([^ {word_chars}]+)")
        self._compactable_pattern = re.compile(f"([{output_charset}]+)")

        self._tokenize_pattern = re.compile(
            f"([{word_chars}]+|[^ {word_chars}]+)"
        )

        self.output_charset = output_charset
        self._out_charset_map: Mapping[str, int] = {
            c: i for i, c in enumerate(output_charset)
        }

        self._charset_map: Mapping[str, int] = {
            c: i for i, c in enumerate(charset)}

        self._rev_out_charset_map = {v: k for k,
                                     v in self._out_charset_map.items()}

    def find_unencodable_chars(self, s: str) -> List[str]:
        """
        returns the list of caracters that cannot be encoded
        """
        allowed: Set[str] = set(self.charset)
        tested: Set[str] = set(s)
        return list(tested - allowed)

    def can_encode(self, s: str) -> bool:
        """
        can_encode the string? if the string has the caracters and only the
        symbols in the charset, this method returns True
        """
        unencodables: List[str] = self.find_unencodable_chars(s)
        return len(unencodables) == 0

    def is_word(self, s: str) -> bool:
        if not len(s):
            return False
        else:
            return self._wordtoken_pattern.fullmatch(s) is not None

    def is_nonword(self, s: str) -> bool:
        if not len(s):
            return False
        else:
            return self._nonwordtoken_pattern.fullmatch(s) is not None

    def is_compactable(self, s: str) -> bool:
        if not len(s):
            return False
        if self._compactable_pattern.fullmatch(s) is not None:
            return True
        return False

    def is_first_letter_compactable(self, s: str) -> bool:
        if not len(s):
            return False
        else:
            return s[0] in self.output_charset

    def to_ints(self, s: str) -> List[int]:
        """
        take a string and convert it to its decoded format aka a list of
        positions based on the map self.output_charset
        """

        return [self._out_charset_map[c] for c in s]

    def to_litteral_ints(self, s: str) -> List[int]:
        return [self._charset_map[c] for c in s]

    def tokenize(self, string: str) -> List[str]:
        """
        splits a text stream
        assuming word has no spaces... will split the word into symbol tokens
        the idea is to save up space and compress words and
        symbols/punctuation separately
        """
        tokens: List[str] = []
        for match in self._tokenize_pattern.finditer(string):
            token: str = str(match.group()).strip()
            if not token:
                continue
            tokens.append(token)

        return tokens

    def new_text_data(self) -> TextData:
        return TextData(self)

    def __repr__(self) -> str:
        return f"CodePage({self.name})"


class TextData:
    """
    prepare text data for text analysis
    """

    def __init__(self, codepage: CodePage):
        self._codepage: CodePage = codepage

        self._raw_lines: List[str] = []
        self._tokenized_lines: List[List[str]] = []

    def append_text(self, textlines: Iterable[str]) -> None:
        self._raw_lines.extend(textlines)
        for line in textlines:
            tokens: List[str] = self._codepage.tokenize(line)
            self._tokenized_lines.append(tokens)

    def tokens(self) -> Generator[str, None, None]:
        for line in self._tokenized_lines:
            for token in line:
                yield token

    def codepage(self) -> CodePage:
        return self._codepage


class LatinCodePage(CodePage):
    def __init__(self) -> None:
        super().__init__(
            name="LATIN",

            charset=" !\"#$%&'()*+,-./"
            "0123456789:;<=>?"
            "@ABCDEFGHIJKLMNO"
            "PQRSTUVWXYZ[\\]^_"
            "`abcdefghijklmno"
            "pqrstuvwxyz{|}~"
            "¡¢£€¥Š§š©ŽµžŒœŸ¿"
            "ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏ"
            "ÑÒÓÔÕÖÙÚÛÜÝ"
            "àáâãäåçèéêëìíîï"
            "ñòóôõöùúûüýÿ",

            output_charset="!\"#$%&'()*+,-./"
            "0123456789:;<=>?"
            "`abcdefghijklmno"
            "pqrstuvwxyz{|}~"
            "¡¢@[\\]§š©ª«¬®¯"
            "^_²³Žµ¶·ž¹º»ŒœŸ¿"
            "àáâãäåæçèéêëìíîï"
            "ðñòóôõö÷øùúûüýþÿ",

            word_chars="$@%0123456789:ABCDEFGHIJKLMNOPQRSTUVWXYZ!?"
            "abcdefghijklmnopqrstuvwxyzÀÁÂÃÄÅÆÇÈÉÊË"
            "ÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñ"
            "òóôõö÷øùúûüýþÿ'-Œœ",
        )


def guess_codepage(s: str) -> CodePage:
    """
    factory method trying to guess what codepage could be used to encode the
    string provision

    NOTE : version 1, we work with LATIN only
    """
    joined: str = " ".join(" ".join(s).split())
    codepage: CodePage = LatinCodePage()

    if not codepage.can_encode(joined):
        unencodable_chars: List[str] = codepage.find_unencodable_chars(joined)
        raise CodePageError(
            f"codepage `{codepage.name}` can't "
            f"encode data because chars {unencodable_chars} not in the charset"
        )

    logger.info(f"using {codepage}")

    return codepage
