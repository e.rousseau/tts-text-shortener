from typing import List, Literal, SupportsIndex


def search_sub_array(src: List, sub_array: List) -> List[int]:
    """
    find all sub arrays and return their position as a list
    """
    if len(src) < len(sub_array):
        return []

    result: List[int] = []
    skip: int = 0

    for i in range(len(src) - len(sub_array) + 1):
        if skip:
            skip -= 1
            continue

        for j, data in enumerate(sub_array):
            if data != src[i + j]:
                break
        else:
            # we've got a match
            skip = len(sub_array) - 1
            result.append(i)

    return result


def ints_to_bytes(
    data: List[int],
    length: SupportsIndex,
    byteorder: Literal["little", "big"],
    signed: bool = False,
) -> List[int]:
    """
    converts a list of integer to a list of bytes. encoding is defined by the
    arguments. Length is the length in bytes of the output value.
    """
    byte_list: List[int] = []

    for i in data:
        b: bytes = i.to_bytes(length, byteorder=byteorder, signed=signed)
        byte_list.extend(b)

    return byte_list


class BinBuffer(List[int]):
    def append_byte_array(self, ba: bytearray) -> None:
        for b in ba:
            self.append(b)

    def append8(self, u8: int) -> None:
        if u8 < 0 or u8 > 255:
            raise ValueError("out of bound value")
        self.append(u8)

    def append16(self, u16: int) -> None:
        if u16 < 0 or u16 > 0xFFFF:
            raise ValueError("out of bound value")

        b1 = (u16 & 0xFF00) >> 8
        b2 = u16 & 0xFF

        self.append(b1)
        self.append(b2)
