from datetime import date
import logging
import re
import csnake
from csnake.codewriter import CodeWriter

def escape(name):
    return re.sub('[^_0-9A-Za-z]', r'_', name)

class CLangModule(CodeWriter):
    def __init__(self, filename:str):
        super().__init__()
        self.filename:str = filename
        self._start()
    
    def _start(self):
        license_text = f"""The content of this file is copyright and shouldn't be distributed in any
manners without the consent of it's authors.
"""
        intro_text = f"""This file was automatically generated using csnake v{csnake.__version__}.

This file should not be edited directly, any changes will be
overwritten next time the script is run.

Source code for csnake is available at:
https://gitlab.com/andrejr/csnake
"""
        authors = [
            {"name": "Emmanuel Rousseau", "email": "e.rousseau@kolabnow.com"},
        ]
        year = date.today().year
    
        self.add_license_comment(
            license_text, authors=authors, intro=intro_text, year=year
        )

    def write(self):
        self._end()
        logging.info(f"Write {self.filename}")
        self.write_to_file(self.filename)

    def _end(self): pass
