import re
from datetime import date
import logging
import csnake
from csnake import Variable, Struct, Function
from csnake.codewriter import CodeWriter, Enum
import os
from typing import Union


class CLangHeader:
    def __init__(self, filename: str):
        self.filename: str = filename
        self.cw: csnake.CodeWriter = CodeWriter()
        self._start()

    def _start(self):
        license_text = f"""The content of this file is copyright and shouldn't be distributed in any
manners without the consent of it's authors.
"""
        intro_text = f"""This file was automatically generated using csnake v{csnake.__version__}.

This file should not be edited directly, any changes will be
overwritten next time the script is run.

Source code for csnake is available at:
https://gitlab.com/andrejr/csnake
"""
        authors = [
            {"name": "Emmanuel Rousseau", "email": "e.rousseau@kolabnow.com"},
        ]
        year = date.today().year

        self.cw.add_license_comment(
            license_text, authors=authors, intro=intro_text, year=year
        )

        filename: str = os.path.basename(self.filename)
        header_guard_name = re.sub(
            "[^_0-9A-Za-z]", r"_", f"_{filename}").upper()
        self.cw.start_if_def(header_guard_name, True)
        self.cw.add_define(header_guard_name)

    def comment_doxygen(self, comment: str):
        self.cw.add_line(f"/// {comment}")

    def comment_inline(self, comment: str):
        self.cw.add_line(f"// {comment}")

    def declare(self, variable: Union[Variable, Function]):
        if isinstance(variable, Function):
            self.cw.add_function_prototype(variable, True)
        elif isinstance(variable, Variable):
            self.cw.add_variable_declaration(variable, extern=True)
        else:
            raise ValueError(f"not supported type {type(variable)}")

    def define(self, defname, value, comment=None):
        if comment:
            self.cw.add_line(f"/// {comment}")
        self.cw.add_define(defname, value)

    def initialize(self, variable: Variable):
        self.cw.add_variable_initialization(variable)

    def include(self, file):
        self.cw.include(file)

    def add_enum(self, enum: Enum):
        self.cw.add_enum(enum)

    def add_struct(self, struct: Struct):
        self.cw.add_struct(struct)

    def add_lines(self, lines):
        self.cw.add_lines(lines)

    def write(self):
        self._end()
        logging.info(f"Write {self.filename}")

        self.cw.write_to_file(self.filename)

    def get_include_filename(self):
        return os.path.basename(self.filename)

    def __str__(self):
        return self.get_include_filename()

    def _end(self):
        self.cw.end_if_def()
