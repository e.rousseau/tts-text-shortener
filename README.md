# TTS : TTS Text Shortener

TTS is a text compression utility implementing a custom algorithm. 
Thus, a clever one would infer immediately how mediocre this algorithm 
is for deflating anything but text.

This python project is the reference implementation, and can be used as a 
base to compress text.

## How to 

You need python 3.8+ installed with `pip` and `setuptools`. Normaly, that come
out of the box when installing a modern python interpreter.

I'd strongly advocate that you run this app from within a `venv`. If you don't
know what it is, you can skip this step, no danger.

Just clone the repository and run:

    $ pip install --upgrade setuptools
    $ python setup.py build

Then,

    $ python setup.py install

Then you'll be able to compress a text file using the syntax :

    $ ttsc input.txt --output output.bin

The input text file must contain singles strings, one per line. The file
idealy should be encoded in UTF-8 and should use the base Latin-1 charset as
defined by `ISO8859-15`. 

Additionnal charset are going to be supported later.

Enjoy!

## TTS - a shorthand for Tasty Text Shortening

Seriously, TTS produce a TTS dataset, aka, Tasty Text Shortening.

The algorithm is intended for retro game programming in limited context:

 1. the ROM space is limited
 2. CPU is maybe slow 
 3. very limited RAM capabilities. 
 
Being able to inflate a single random sentence from a big RPG script without 
reserving RAM, directly from the ROM is the main purpose of TTS. 

*WIP readme documentation*
