from setuptools import setup

setup(
    name="ttsc",
    version="0.0.1",
    install_requires=[],
    entry_points={
        'console_scripts': [
            'ttsc = erousseau.tts.ttsc:main',
        ]
    }
)
