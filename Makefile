# just a quick Makefile as a reminder for the setup
# commands

init:
	pip install --upgrade setuptools

install:
	python setup.py install

.PHONY: init
