#!/bin/bash
set -e

rm ./dist/*
python -m build
pip install ./dist/ttsc-0.0.1-py3-none-any.whl --force
cp ./dist/ttsc-0.0.1-py3-none-any.whl ../fatal_maze/tools/
